import {React, Component, useState} from 'react';
import {Text, TouchableWithoutFeedback, View, StyleSheet} from 'react-native';
export default class PhoneInput extends Component {
    const [count, setCount] = useState(0);
    const onPress = () => {
        setCount(count + 1);
    };
    const styles = StyleSheet.create({
        
    });
    render(){
        return (
            <View>
                <View>
                    <Text>Count {count}</Text>
                </View>
                <TouchableWithoutFeedback>
                    <View>
                        <Text>Click me</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}
